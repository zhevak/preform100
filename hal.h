// hal.h


#ifndef __HAL_H__
#define __HAL_H__


#ifdef __cplusplus
 extern "C" {
#endif


void hal_init(void);    // Инициализация уровня абстрагирования от "железа"


#ifdef __cplusplus
}
#endif


#endif
